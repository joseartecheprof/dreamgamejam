using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    public Rigidbody myMotorSphereRb;

    public float moveInput;
    public float turnInput;
    public float fwdSpeed = 200f;
    public float revertSpeed = 100f;

    public float turnSpeed=150f;
    void Start()
    {
        myMotorSphereRb.transform.parent = null;
    }

    // Update is called once per frame
    void Update()
    {
        moveInput = Input.GetAxisRaw("Vertical");
        turnInput = Input.GetAxisRaw("Horizontal");
        moveInput *= moveInput > 0? fwdSpeed:revertSpeed;
       

        transform.position = myMotorSphereRb.position;
        float turnMultiplier = 1f;
        if (Input.GetAxisRaw("Vertical") < 0) turnMultiplier = -1;
        if(Input.GetAxisRaw("Vertical") ==0 || myMotorSphereRb.velocity.magnitude < 0.05f) { turnMultiplier = 0; }
        float turnRotation = turnInput * turnSpeed * Time.deltaTime * turnMultiplier;
        transform.Rotate(0, turnRotation, 0, Space.World);
    }

    private void FixedUpdate()
    {
        myMotorSphereRb.AddForce(transform.forward * moveInput , ForceMode.Acceleration);
    }

}
